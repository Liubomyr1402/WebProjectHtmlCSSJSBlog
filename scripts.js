function AddingSkills(id) {

    var progresses = document.getElementsByClassName("ourProgresses");

    switch(id.id)
    {
        case 'addHtmlKnowings':
        {
            progresses[0].value += 1;
            document.getElementById("txtHtmlKnowing").innerHTML = "HTML " + progresses[0].value + "/100";
        }
        break;
        case 'addJSKnowings':
        {
            progresses[1].value += 1;
            document.getElementById("txtJSKnowing").innerHTML = "JS " + progresses[1].value + "/100";
        }
        break;
        case 'addCsharpKnowings':
        {
            progresses[2].value += 1;
            document.getElementById("txtCsharpKnowing").innerHTML = "C# " + progresses[2].value + "/100";
        }
        break;
    }
}

var images = ["images/proj1.PNG", "images/proj2.jpg", "images/proj3.PNG"];
var numberOfImage = 0;

function NextProj() {
    var slider = document.getElementById("slider");
    numberOfImage++;
    if (numberOfImage >= images.length) {
        numberOfImage = 0;
    }
        slider.src = images[numberOfImage];
}

function PreProj() {
    var slider = document.getElementById("slider");
    numberOfImage--;
    if (numberOfImage < 0) {
        numberOfImage = images.length - 1;
    }
    slider.src = images[numberOfImage];
}